{ pkgs ? import <nixpkgs> { inherit system; }, system ? builtins.currentSystem
, dbName ? "", dbUser ? "", dbPassword ? "", dbHost ? "" }:

let
  php = pkgs.php;

  package = import ./default.nix {
    inherit system;
    pkgs = (pkgs // { inherit php; });
    noDev = true;
  };
in {
  package = package.override {
    removeComposerArtifacts = true; # Remove composer configuration files
    postInstall = ''
      [[ ! -z "${dbName}" ]] && sed -ibak "s/\/\* DBName: \*\/ \(\"\\|'\)[^\"']*/\1${dbName}/" $out/public/index.php
      [[ ! -z "${dbUser}" ]] && sed -ibak "s/\/\* User: \*\/ \(\"\\|'\)[^\"']*/\1${dbUser}/" $out/public/index.php
      [[ ! -z "${dbPassword}" ]] && sed -ibak "s/\/\* Pass: \*\/ \(\"\\|'\)[^\"']*/\1${dbPassword}/" $out/public/index.php
      [[ ! -z "${dbHost}" ]] && sed -ibak "s/\/\* Host: \*\/ \(\"\\|'\)[^\"']*/\1${dbHost}/" $out/public/index.php
      rm -f $out/public/index.phpbak
    '';
  };
  rewrites = import ./rewrites.nix;
}
