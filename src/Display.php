<?php

namespace JournalOfDiscourses;

class Display
{
    private $link;
    private $volume;
    private $discourse;
    private $author;
    private $pageTitle;

    public function __construct($getVar, $dbName, $mysqlUser, $mysqlPassword = '', $dbHost = '')
    {
        $dbHost = $dbHost === '' ? 'localhost' : $dbHost;
        if ($dbHost === '.') {
          $this->link = new \mysqli($dbHost, $mysqlUser, $mysqlPassword, $dbName, null, 'mysql');
        } else {
          $this->link = new \mysqli($dbHost, $mysqlUser, $mysqlPassword, $dbName);
        }

        if ($this->link->connect_error) {
            die('Connect Error ('.$this->link->connect_errno.') '
              .$this->link->connect_error);
        }

        $this->volume = isset($getVar['volume']) ? $getVar['volume'] : '';
        if ('' == $this->volume) {
            $this->volume = null;
        }
        $this->discourse = isset($getVar['discourse']) ? $getVar['discourse'] : '';
        if ('' == $this->discourse) {
            $this->discourse = null;
        }
        $this->author = isset($getVar['author']) ? $getVar['author'] : '';
        $this->author = str_replace('_', ' ', $this->author);
        if ('' == $this->author) {
            $this->author = null;
        }

        $numberOfDiscoursesArray = array(54, 56, 51, 70, 62, 66, 58, 91, 76, 68, 56, 69, 47, 49, 46, 46, 50, 48, 53, 48, 41, 51, 43, 44, 43, 40);
        $authorsArray = array(
          'Albert Carrington', 'Alfred Cordon', 'Amasa M. Lyman', 'Angus M. Cannon', 'Aurelius Miner', 'B. H. Roberts',
          'Brigham Young', 'Brigham Young, Jr', 'C. W. Stayner', 'Charles C. Rich', 'Charles W. Penrose', 'Daniel H. Wells',
          'Daniel Spencer', 'David McKenzie', 'Edward Hunter', 'Elias Smith', 'Erastus Snow', 'Ezra T. Benson', 'Francis M. Lyman',
          'Franklin D. Richards', 'George A. Smith', 'George B. Wallace', 'George G. Bywater', 'George Q. Cannon',
          'George Reynolds', 'George Teasdale', 'Heber C. Kimball', 'Henry W. Naisbitt', 'Isaac Morley', 'Jedediah M. Grant',
          'John H. Smith', 'John Morgan', 'John Nicholson', 'John Q. Cannon', 'John Taylor', 'John Young', 'Joseph E. Taylor',
          'Joseph F. Smith', 'Joseph Smith', 'Joseph Young', 'Junius F. Wells', 'L. W. Hardy', 'Lorenzo D. Young', 'Lorenzo Snow',
          'Moses Thatcher', 'Orson F. Whitney', 'Orson Hyde', 'Orson Pratt', 'Parley P. Pratt', 'Wilford Woodruff',
          'Willard Richards', 'William C. Dunbar', 'Z. Snow',
        );

        // Catch any nonsense input
        if (
            (!is_null($this->volume) && ($this->volume < 1 || $this->volume > 26)) ||
            (!is_null($this->discourse) && ($this->discourse < 1 || $this->discourse > $numberOfDiscoursesArray[$this->volume - 1])) ||
            (!is_null($this->author) && ('people' !== $this->author && !in_array($this->author, $authorsArray)))
        ) {
            echo '404';
            exit();
        }

        if (!is_null($this->volume) && is_null($this->discourse)) {
            $this->pageTitle = "Journal of Discourses, volume {$this->volume}";
        } elseif (!is_null($this->volume) && !is_null($this->discourse)) {
            $get_discourse_info_query = "SELECT *
                FROM discourses
                INNER JOIN authors ON discourses.discourse_author_id = authors.author_id
                WHERE discourse_volume_number = {$this->volume}
                AND discourse_number = {$this->discourse}
                LIMIT 1";
            $get_discourse_info_result = mysqli_query($this->link, $get_discourse_info_query);
            if (!$get_discourse_info_result instanceof \mysqli_result) {
                throw new \Error($get_discourse_info_query);
            }
            while ($row = mysqli_fetch_array($get_discourse_info_result)) {
                $this->pageTitle = "{$row['author_name']}: {$row['discourse_header']} (Journal of Discourses)";
            }
        } elseif ('people' == $this->author) {
            $this->pageTitle = 'People';
        } elseif (is_string($this->author)) {
            $this->pageTitle = "Discourses from {$this->author}";
        } else {
            $this->pageTitle = 'Journal of Discourses';
        }
    }

    public function build_page()
    {
        return <<<EOF
            <!DOCTYPE html>
            <html>
            <head>
                <title>{$this->pageTitle}</title>
                <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'>
                <meta http-equiv='content-type' content='text/html;charset=iso-8859-1'/>
                <meta name='description' content='The Journal of Discourses, a collection of sermons by early LDS (Mormon) leaders, is a rich source of early Mormon theology and thinking.'>
                <meta name='keywords' content='Journal of Discourses,journalofdiscourses,Mormon,prophet,Mormonism,LDS,JoD,JD,Brigham Young,history'/>
                <link rel='stylesheet' href='//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css'>
                <style type='text/css'>
                html { font-family:Arial; }
                div.content { margin:0px auto; padding:20px; max-width:960px; }
                div.paragraph { margin:10px 0; text-indent:20px; }
                </style>
            </head>
                <body>
                <div class='content clearfix'>
                    {$this->print_navbar()}
                    {$this->print_breadcrumbs()}
                </div>
                <script src='//code.jquery.com/jquery-2.0.3.min.js' defer></script>
                <script src='//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js' defer></script>
                </body>
            </html>
EOF;
    }

    private function print_breadcrumbs()
    {
        $output = <<<EOF
            <ol class='breadcrumb'>
              <li><a href='/'><span class='glyphicon glyphicon-home'></span> Journal of Discourses</a></li>
EOF;
        switch (true) {
          case 'people' == $this->author:
            $output .= <<<EOF
                <li class='active'><span class='glyphicon glyphicon-user'></span> People</li>
              </ol>
              {$this->print_authors()}
EOF;
          break;
          case !is_null($this->author):
            $output .= <<<EOF
                <li><a href='/people'><span class='glyphicon glyphicon-user'></span> People</a></li>
                <li class='active'><span class='glyphicon glyphicon-user'></span> {$this->author}</li>
              </ol>
              {$this->print_author()}
EOF;
          break;
          case $this->volume > 0 && is_null($this->discourse):
            $get_discourses_query = "SELECT *
              FROM discourses
              INNER JOIN authors ON discourses.discourse_author_id = authors.author_id
              WHERE discourse_volume_number = {$this->volume}";
            $output .= <<<EOF
                <li class='active'><span class='glyphicon glyphicon-book'></span> Volume {$this->volume}</li>
              </ol>
              {$this->print_discourses_index($get_discourses_query)}
EOF;
          break;
          case $this->volume > 0 && $this->discourse > 0:
            $output .= <<<EOF
                <li><a href='/{$this->volume}'><span class='glyphicon glyphicon-book'></span> Volume {$this->volume}</a></li>
                <li class='active'><span class='glyphicon glyphicon-bullhorn'></span> Discourse {$this->discourse}</li>
              </ol>
              {$this->print_discourse()}
EOF;
          break;
          case is_null($this->volume) && is_null($this->discourse):
            return $this->print_home();
        }

        return $output;
    }

    private function dropdown_menu($get_authors_result)
    {
        $output = '';
        $i = 0;
        while ($row = mysqli_fetch_array($get_authors_result)) {
            ++$i;
            $author_name = $row['author_name'];
            $author_link = str_replace(' ', '_', $author_name);
            $author_discourses_count = $row['author_discourses_count'];
            $output .= "<li><a href='/{$author_link}'><span class='glyphicon glyphicon-user'></span> {$author_name} <span class='badge'>{$author_discourses_count} ".(1 == $i ? 'discourses' : '').'</span></a></li>';
        }

        return $output;
    }

    private function volume_links()
    {
        $output = '';
        for ($i = 1; $i <= 26; ++$i) {
            $output .= "<li><a href='/{$i}'><span class='glyphicon glyphicon-book'></span> Volume {$i}</a></li>";
        }

        return $output;
    }

    private function print_navbar()
    {
        $get_authors_query = 'SELECT *
            FROM authors
            WHERE author_discourses_count > 0
            ORDER BY author_discourses_count DESC';
        $get_authors_result = mysqli_query($this->link, $get_authors_query);

        return <<<EOF
            <nav class='navbar navbar-default' role='navigation'>
                <div class='navbar-header'>
                <button type='button' class='navbar-toggle' data-toggle='collapse' data-target='#bs-example-navbar-collapse-1'>
                    <span class='sr-only'>Toggle navigation</span>
                    <span class='icon-bar'></span>
                    <span class='icon-bar'></span>
                    <span class='icon-bar'></span>
                </button>
                <a class='navbar-brand' href='/'>Journal of Discourses</a>
                </div>
                <div class='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>
                <ul class='nav navbar-nav'>
                    <li class='dropdown'>
                    <a href='#' class='dropdown-toggle' data-toggle='dropdown'><span class='glyphicon glyphicon-user'></span> People <b class='caret'></b></a>
                    <ul class='dropdown-menu'>
                        {$this->dropdown_menu($get_authors_result)}
                    </ul>
                    </li>
                    <li class='dropdown'>
                    <a href='#' class='dropdown-toggle' data-toggle='dropdown'><span class='glyphicon glyphicon-book'></span> Volumes <b class='caret'></b></a>
                    <ul class='dropdown-menu'>
                        {$this->volume_links()}
                    </ul>
                    </li>
                </ul>
                </div>
            </nav>
EOF;
    }

    private function print_home()
    {
        return <<<EOF
          <blockquote>
            <p>The Journal of Discourses deservedly ranks as one of the standard works of the Church, and every rightminded Saint will certainly welcome with joy every number (issue) as it comes forth.</p>
            <small>President George Q. Cannon, Journal of Discourses, Preface, Volume 8.</small>
          </blockquote>
          <blockquote>
            <p>Each successive Volume of these Discourses is a rich mine of wealth, containing gems of great value, and the diligent seeker will find ample reward for his labor. After the fathers and mothers of this generation have made them the study of their lives their children's children will find that they are still unexhausted, and rejoice that this Record has been handed down from their fathers to also aid them in following the way of life.</p>
            <small>Apostle Orson Pratt, Preface. Volume 3.</small>
          </blockquote>
          <blockquote>
            <p>It is impossible to give monetary value to the past volumes of this publication, ... Those who read the utterances of the servants of God, contained in this book, under the same influence by which the speakers were inspired, cannot fail to receive profit from the perusal.</p>
            <small>President Joseph F. Smith, Preface, Volume 18.</small>
          </blockquote>
          <blockquote>
            <p>We take great pleasure in presenting to the Saints and the world ... the Journal of Discourses, which they will find contains rich treasures of information concerning the glorious principles of Eternal Life, as revealed through God's anointed servants in these last days. All who read the discourses contained in this Volume are earnestly recommended to adapt them to their lives by practice, and we can confidently assure them that, in doing so, they are laying up a store of knowledge that will save and exalt them in the Celestial kingdom.</p>
            <small>Apostle Albert Carrington, Journal of Discourses, Preface, Volume 15.</small>
          </blockquote>
EOF;
    }

    private function print_discourses_index($get_discourses_query)
    {
        $output = '';
        $get_discourses_result = mysqli_query($this->link, $get_discourses_query);
        if (!$get_discourses_result instanceof \mysqli_result) {
            throw new \Error($get_discourses_query);
        }
        while ($row = mysqli_fetch_array($get_discourses_result)) {
            $discourse_volume_number = $row['discourse_volume_number'];
            $discourse_number = $row['discourse_number'];
            $discourse_title = $row['discourse_title'];
            $discourse_header = $row['discourse_header'];
            $discourse_subtitle = $row['discourse_subtitle'];
            $author_id = $row['discourse_author_id'];
            $discourse_first_page = $row['discourse_first_page'];
            $discourse_last_page = $row['discourse_last_page'];
            if ('' == $row['author_image']) {
                $author_image_path = '/images/default.jpg';
            } else {
                $author_image_path = "/images/thumbnails/{$author_id}.jpg";
            }
            $output .= <<<EOF
              <div class='media'>
                <a class='pull-left'>
                  <img class='media-object' width='80' height='80' src='{$author_image_path}'>
                </a>
                <div class='media-body'>
                  <h4 class='media-heading'><a href='/{$discourse_volume_number}/{$discourse_number}'>{$discourse_header}</a></h4>
                  <p>{$discourse_subtitle}</p>
                  <p style='font-size:12px;'>Volume {$discourse_volume_number}, discourse {$discourse_number}, pages {$discourse_first_page}-{$discourse_last_page}</p>
                </div>
              </div>
EOF;
        }

        return $output;
    }

    private function print_navigation_authors($get_authors_result)
    {
        $output = '';
        while ($row = mysqli_fetch_array($get_authors_result)) {
            $author_name = $row['author_name'];
            $author_name_underscored = str_replace(' ', '_', $author_name);
            $author_discourses_count = $row['author_discourses_count'];
            $output .= "<li class='discourse_li' volume_number='authors'><a href='/{$author_name_underscored}'>{$author_name} ({$author_discourses_count})</a></li>";
        }

        return $output;
    }

    private function print_navigation_volumes($get_discourses_result)
    {
        $output = '';
        $last_volume = null;
        $is_not_first_iteration = false;
        while ($row = mysqli_fetch_array($get_discourses_result)) {
            $volume_number = $row['discourse_volume_number'];
            if ($volume_number !== $last_volume) {
                if ($is_not_first_iteration) {
                    $output .= '</ul>';
                }
                $output .= "<li class='volume_li' volume_number='{$volume_number}'>Volume {$volume_number}</li>";
                $output .= "<ul class='discourses_ul' volume_number='{$volume_number}'>";
            }
            $discourse_header = $row['discourse_header'];
            $discourse_number = $row['discourse_number'];
            $output .= "<li class='discourse_li' volume_number='{$volume_number}' style='whitespace:no-wrap;'>";
            $output .= "<a href='/{$volume_number}/{$discourse_number}'>{$discourse_number}. {$discourse_header}</a>";
            $output .= '</li>';
            $last_volume = $volume_number;
            $is_not_first_iteration = true;
        }

        return $output;
    }

    private function print_navigation()
    {
        $get_authors_query = 'SELECT *
          FROM authors
          WHERE author_discourses_count > 0
          ORDER BY author_discourses_count DESC';
        $get_authors_result = mysqli_query($this->link, $get_authors_query);
        $get_discourses_query = 'SELECT * FROM discourses';
        $get_discourses_result = mysqli_query($this->link, $get_discourses_query);

        return <<<EOF
          <div class='navigation'>
            <ul>
              <li class='volume_li' volume_number='authors'>People</li>
              <ul class='discourses_ul' volume_number='authors'>
                {$this->print_navigation_authors($get_authors_result)}
              </ul>
              {$this->print_navigation_volumes($get_discourses_result)}`
            </ul>
          </div>
EOF;
    }

    private function print_discourse()
    {
        $get_discourse_query = "SELECT * 
          FROM discourses
          INNER JOIN authors ON discourses.discourse_author_id = authors.author_id
          WHERE discourse_volume_number = {$this->volume}
          AND discourse_number = {$this->discourse}
          LIMIT 1";
        $output = '';
        $get_discourse_result = mysqli_query($this->link, $get_discourse_query);
        if (!$get_discourse_result instanceof \mysqli_result) {
            throw new \Error($get_discourse_query);
        }
        while ($row = mysqli_fetch_array($get_discourse_result)) {
            $author_id = $row['author_id'];
            $author_name = $row['author_name'];
            $author_name_underscored = str_replace(' ', '_', $author_name);
            $author_bio = $row['author_bio'];
            $discourse_title = $row['discourse_title'];
            $discourse_content = $row['discourse_content'];
            $output .= <<<EOF
              <div>
                <div class='page-header'>
                  <h1>{$discourse_title} <small>{$author_name}</small></h1>
                </div>
                <div>{$discourse_content}</div>
                <div>- {$author_name}</div>
              </div>
EOF;
        }

        return $output;
    }

    private function print_author()
    {
        $output = '';
        $get_author_query = "SELECT *
          FROM authors
          WHERE author_name = '{$this->author}'
          LIMIT 1";
        $get_author_result = mysqli_query($this->link, $get_author_query);
        if (!$get_author_result instanceof \mysqli_result) {
            throw new \Error($get_author_query);
        }
        $author_bio = '';
        $author_id = null;
        while ($row = mysqli_fetch_array($get_author_result)) {
            $author_id = $row['author_id'];
            $author_bio = $row['author_bio'];
        }

        $output .= "<div class='page-header'><h1>{$this->author}</h1></div>";

        if ('' !== $author_bio) {
            $output .= "<div class='well'>{$author_bio}</div>";
        }

        $get_author_discourses_query = "SELECT *
          FROM discourses
          INNER JOIN authors ON discourses.discourse_author_id = authors.author_id
          WHERE discourse_author_id = {$author_id}";

        return $output.$this->print_discourses_index($get_author_discourses_query);
    }

    private function print_authors()
    {
        $output = '';
        $get_authors_query = 'SELECT * 
          FROM authors
          WHERE author_discourses_count > 0
          ORDER BY author_discourses_count DESC';
        $get_authors_result = mysqli_query($this->link, $get_authors_query);
        if (!$get_authors_result instanceof \mysqli_result) {
            throw new \Error($get_authors_query);
        }
        while ($row = mysqli_fetch_array($get_authors_result)) {
            $author_id = $row['author_id'];
            $author_name = $row['author_name'];
            $author_link = str_replace(' ', '_', $author_name);
            $author_bio = $row['author_bio'];
            if ('' == $row['author_image']) {
                $author_image_path = '/images/default.jpg';
            } else {
                $author_image_path = "/images/thumbnails/{$author_id}.jpg";
            }
            $output .= <<<EOF
              <div class='media'>
                <a class='pull-left'>
                  <img class='media-object' width='80' height='80' src='{$author_image_path}'>
                </a>
                <div class='media-body'>
                  <h4 class='media-heading'><a href='/{$author_link}'>{$author_name}</a></h4>
                  {$author_bio}
                </div>
              </div>
EOF;
        }

        return $output;
    }
}
