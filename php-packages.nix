{composerEnv, fetchurl, fetchgit ? null, fetchhg ? null, fetchsvn ? null, noDev ? false}:

let
  packages = {};
  devPackages = {
    "svanderburg/composer2nix" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "svanderburg-composer2nix-57cecaf5d9d667b47415bb7c1d1f5154be7c759e";
        src = fetchurl {
          url = https://api.github.com/repos/svanderburg/composer2nix/zipball/57cecaf5d9d667b47415bb7c1d1f5154be7c759e;
          sha256 = "0s6fjwaf2dwzf9h83dms5wg8s3a1kcy5nmdnn7wy1ykqi3mhp61m";
        };
      };
    };
    "svanderburg/pndp" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "svanderburg-pndp-4bfe9c4120c23354ab8dc295957dc3009a39bff0";
        src = fetchurl {
          url = https://api.github.com/repos/svanderburg/pndp/zipball/4bfe9c4120c23354ab8dc295957dc3009a39bff0;
          sha256 = "0n2vwpwshv16bhb7a6j95m664zh4lpfa7dqmcyhmn89nxpgvg91y";
        };
      };
    };
  };
in
composerEnv.buildPackage {
  inherit packages devPackages noDev;
  name = "nocoolnametom-journalofdiscourses";
  src = ./.;
  executable = false;
  symlinkDependencies = false;
  meta = {};
}